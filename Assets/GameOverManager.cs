﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
	public static GameOverManager Instance;
	public Camera GameOverCamera;
	public TextMeshProUGUI CashText;
	public Button restartButton;
	public Button menuButton;
	void Start ()
	{
		Instance = this;
		restartButton.onClick.AddListener(RestartButtonHandler);
		menuButton.onClick.AddListener(MenuButtonHandler);
	}

	private void MenuButtonHandler()
	{
		SceneManager.LoadScene(0);
	}

	private void RestartButtonHandler()
	{
		SceneManager.LoadScene(1);
	}

	public void SetUpGameOver(int score)
	{
		CashText.text = "Cash: " + score.ToString() + "$";
	}
}
