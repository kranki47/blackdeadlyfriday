﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioSystem : MonoBehaviour {

    public static AudioSystem Instance;

    public AudioMixerGroup SFXGroup;
    public AudioMixerGroup VoiceGroup;
    public AudioMixerGroup BackgroundMusicGroup;

    public AudioClip BackgroundMusicClip;
	[SerializeField]
    private List<AudioSource> _sfxSources  = new List<AudioSource>();
	[SerializeField]
    private List<AudioSource> _voiceSources = new List<AudioSource>();

	public void Awake()
    {
        Instance = this;

        for (int i = 0; i < 5; i++)
        {
            _sfxSources.Add(gameObject.AddComponent<AudioSource>());
            _sfxSources[i].outputAudioMixerGroup = SFXGroup;
            _voiceSources.Add(gameObject.AddComponent<AudioSource>());
            _voiceSources[i].outputAudioMixerGroup = VoiceGroup;
        }
    }

    public void StopAll()
    {
        StopCoroutine(RunnersManager.Instance.PlayVoiceActs());
        for (int i = 0; i < _sfxSources.Count; i++)
        {
            if (_sfxSources[i].isPlaying)
            {
                _sfxSources[i].Stop();
            }
        }

	    for (int i = 0; i < _voiceSources.Count; i++)
	    {
		    if (_voiceSources[i].isPlaying)
		    {
			    _voiceSources[i].Stop();
		    }
	    }
    }

    public void PlayOneTime(AudioClip clip, Channel channel)
    {
        switch (channel)
        {
            case Channel.SFX:
                for (int i = 0; i < _sfxSources.Count; i++)
                {
                    if (!_sfxSources[i].isPlaying)
                    {
                        _sfxSources[i].PlayOneShot(clip);
                        break;
                    }
                }
                var source = gameObject.AddComponent<AudioSource>();
                _sfxSources.Add(source);
                source.PlayOneShot(clip);
                break;

            case Channel.Voice:
                for (int i = 0; i < _voiceSources.Count; i++)
                {
                    if (!_voiceSources[i].isPlaying)
                    {
                        _voiceSources[i].PlayOneShot(clip);
                        break;
                    }
                }
                var sourceVoice = gameObject.AddComponent<AudioSource>();
	            _voiceSources.Add(sourceVoice);
                sourceVoice.PlayOneShot(clip);
                break;
        }

    }
}

public enum Channel
{
    SFX,
    Voice
}
