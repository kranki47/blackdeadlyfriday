﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSystem : MonoBehaviour {

    public static PowerUpSystem Instance;
    public System.Action<PowerUp> PowerUpPicked;
    public System.Action PowerUpUsed;

    public PowerUp CurrentPowerUp;
    public bool CanPickUpPowerUp = true;

	// Use this for initialization
	void Awake () 
    {
        Instance = this;
        PowerUpPicked += OnPowerUpPicked;
        PowerUpUsed += OnPowerUpUsed;
	}
	

    public void ActivatePowerUp(PowerUp powerUp, GameObject selectedObject = null)
    {
        switch (powerUp.PowerUpType)
        {
            case PowerUpType.PowerCut:
                for (int i = 0; i < RunnersManager.Instance.runners.Count; i++)
                {
                    RunnersManager.Instance.runners[i].TurnLightOff();
                }
                break;
            case PowerUpType.AdrenalineRush:
                if (selectedObject)
                {
                    StartCoroutine(selectedObject.GetComponent<Runner>().ActivateAdrenaline());
                }
                break;

            case PowerUpType.Chainlight:
                if (selectedObject)
                {
                   Collider[] cols = Physics.OverlapSphere(selectedObject.transform.position, 4.5f);
                    for (int i = 0; i < cols.Length; i++)
                    {
                        if(cols[i].tag == "Runner")
                        cols[i].GetComponent<Runner>().TurnLightOff();
                    }
                }
                break;

            case PowerUpType.Flashbang:
                if (selectedObject)
                {
                    StartCoroutine(selectedObject.GetComponent<Sniper>().HitWithFlashbang());
                }
                break;

            case PowerUpType.LazyRush:
                for (int i = 0; i < RunnersManager.Instance.runners.Count; i++)
                {
                    StartCoroutine(RunnersManager.Instance.runners[i].ActivateLazyRush());
                }
                break;

            case PowerUpType.DropItems:

                break;

            case PowerUpType.Misdirection:
                for (int i = 0; i < RunnersManager.Instance.runners.Count; i++)
                {
                    StartCoroutine(RunnersManager.Instance.runners[i].ActivateMisdirection());
                }
                break;

        }
        PowerUpUsed();
    }

    private void OnPowerUpPicked(PowerUp powerUp)
    {
        if(powerUp.IsCollectable == false)
        {
            ActivatePowerUp(powerUp);
        }
        else
        {
            CanPickUpPowerUp = false;
            CurrentPowerUp = powerUp;
        }
    }
    private void OnPowerUpUsed()
    {
        CanPickUpPowerUp = true;
    }
}
