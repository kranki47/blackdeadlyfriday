﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpButton : MonoBehaviour {

    public PowerUp PowerUp;

    public void Awake()
    {
        GetComponent<Button>().onClick.AddListener(OnAbilityButtonClick);
    }

    // Use this for initialization
    void Start () 
    {
        GetComponent<Image>().sprite = PowerUp.PowerUpSprite;
	}
	
    public void OnAbilityButtonClick()
    {
        if(GameManager.Instance.CurrentInputState == InputState.ApplyPowerUp)
        {
            GameManager.Instance.CurrentInputState = InputState.Default;
        }
        else if( GameManager.Instance.CurrentInputState == InputState.Default)
        {
            if (PowerUp.IsSelectable)
            {
                switch (PowerUp.PowerUpType)
                {
                    case PowerUpType.Flashbang:
                        SniperTargetManager.Instance.TickOutline(true);
                        break;
                    case PowerUpType.AdrenalineRush:
                    case PowerUpType.Chainlight:
                        RunnersManager.Instance.TickOutline(true);
                        break;
                }
                GameManager.Instance.CurrentInputState = InputState.ApplyPowerUp;
            }
            else
            {
                PowerUpSystem.Instance.ActivatePowerUp(PowerUp);
            }
        }
    }
}
