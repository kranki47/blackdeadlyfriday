﻿using System.Collections.Generic;
using UnityEngine;


public class RunnersObjectPoolScript : MonoBehaviour
{
    public static RunnersObjectPoolScript Instance;
    [HideInInspector]
    public List<GameObject> runners;

    [SerializeField]
    private int resourcesPooledAmount = 9;
    [SerializeField]
    private Runner runnerPrefab;
    [SerializeField]
    private bool shouldExpand = true;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        runners = new List<GameObject>();

        for (int i = 0; i < resourcesPooledAmount; i++)
        {
            GameObject runnerObject = Instantiate(runnerPrefab).gameObject;
            runnerObject.gameObject.SetActive(false);
            runners.Add(runnerObject);
        }
    }

    public void HideAllObjects()
    {
        foreach (GameObject pooledObject in runners)
        {
            pooledObject.gameObject.SetActive(false);
        }
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < runners.Count; i++)
        {
            if(runners[i].tag == "Runner" && !runners[i].activeInHierarchy)
            {
                return runners[i];
            }
        }
        for (int i = 0; i < runners.Count; i++)
        {
            if (runners[i].tag == "DeadRunner")
            {
                RunnersManager.Instance.runners.Add(runners[i].GetComponent<Runner>());
                runners[i].SetActive(false);
                return runners[i].gameObject;
            }
        }

        if (shouldExpand)
        {
            GameObject newObjectToPool = Instantiate(runnerPrefab).gameObject;
            newObjectToPool.gameObject.SetActive(false);
            runners.Add(newObjectToPool);
            RunnersManager.Instance.runners.Add(newObjectToPool.GetComponent<Runner>());
            return newObjectToPool.gameObject;
        }
        else
        {
            return null;
        }
    }
}