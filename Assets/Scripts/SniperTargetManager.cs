﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperTargetManager : MonoBehaviour {

    public static SniperTargetManager Instance;
    private const string _runnerLayerMask = "Runner";

    [SerializeField] private List<Sniper> _snipers;

	// Use this for initialization
	void Awake() 
    {
        Instance = this;
	}

    public void TickOutline(bool value)
    {
        for (int i = 0; i < _snipers.Count; i++)
        {
            _snipers[i].transform.GetChild(0).GetComponent<Outline>().enabled = value;
        }
    }

	public void HideSnipers()
	{
		foreach (Sniper sniper in _snipers)
		{
			sniper.gameObject.SetActive(false);
		}
	}

    public GameObject GetNearestValidTarget(Sniper sniper, float sightRadius)
    {
        Collider[] overlappedRunners = Physics.OverlapSphere(sniper.transform.position, sightRadius);
        bool isValidTarget = true;
        for (int i = 0; i < overlappedRunners.Length; i++)
        {
            if(overlappedRunners[i].tag == _runnerLayerMask && overlappedRunners[i].GetComponent<Runner>().GetBrightnessValue() > 0)
            {
                for (int x = 0; x < _snipers.Count; x++)
                {
                    if (_snipers[x] != sniper)
                    {
                        if(_snipers[x].GetCurrentTarget() == overlappedRunners[i].gameObject)
                        {
                            isValidTarget = false;
                        }
                    }
                }
                if (isValidTarget)
                {
                    return overlappedRunners[i].gameObject;
                }
            }
        }
        return null;
    }
}
