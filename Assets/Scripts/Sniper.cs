﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Sniper : MonoBehaviour {

    [SerializeField] private float _sightRadius;
    [SerializeField] private float _aimDuration;
    [SerializeField] private float _shootInterval;

    private Coroutine _lookForTargetCoroutine;

    private Coroutine _checkIfTargetInSightCoroutine;
    private Coroutine _aimCoroutine;
    private GameObject _currentTarget;
    private LineRenderer _lineRenderer;

    public AudioClip[] ShotClips;

	// Use this for initialization
	void Start () 
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.SetPosition(0, transform.position);
        _lineRenderer.SetPosition(1, transform.position);
        _lineRenderer.enabled = false;
        _lookForTargetCoroutine = StartCoroutine(TryGetNearestRunner());
	}

    public GameObject GetCurrentTarget()
    {
        return _currentTarget;
    }

    public void OnMouseDown()
    {
        if(GameManager.Instance.CurrentInputState == InputState.ApplyPowerUp)
        {
            SniperTargetManager.Instance.TickOutline(false);
            PowerUpSystem.Instance.ActivatePowerUp(PowerUpSystem.Instance.CurrentPowerUp, gameObject);
            GameManager.Instance.CurrentInputState = InputState.Default;
        }
    }

    public IEnumerator HitWithFlashbang()
    {
        var defRadius = _sightRadius;
        _sightRadius = 0;
        yield return new WaitForSeconds(2f);
        _sightRadius = defRadius;
    }

    private IEnumerator TryGetNearestRunner()
    {
        while (true)
        {
            _currentTarget = SniperTargetManager.Instance.GetNearestValidTarget(this, _sightRadius);
            if(_currentTarget != null)
            {
                _aimCoroutine = StartCoroutine(Aim());
                if(_checkIfTargetInSightCoroutine == null)
                {
                    _checkIfTargetInSightCoroutine = StartCoroutine(CheckIfTargetOutOfSight());
                }
                _lookForTargetCoroutine = null;
                yield break;
            }
            yield return new WaitForSeconds(0.05f);
        }
    }

    private IEnumerator Aim()
    {
        _lineRenderer.enabled = true;


        float lineWidth = 0f;
        for (float i = 0; i < _aimDuration; i += Time.deltaTime)
        {
            if(_currentTarget == null)
            {
                break;
            }
			transform.LookAt(_currentTarget.transform.position);
            _lineRenderer.SetPosition(1, _currentTarget.transform.position);
            lineWidth += Time.deltaTime / 5;
            _lineRenderer.SetWidth(lineWidth, lineWidth);
            yield return null;
        }

        if (_currentTarget)
        {
            if (_checkIfTargetInSightCoroutine != null)
            {
                StopCoroutine(_checkIfTargetInSightCoroutine);
                _checkIfTargetInSightCoroutine = null;
            }
            AudioSystem.Instance.PlayOneTime(ShotClips[Random.Range(0,2)], Channel.SFX);
            _currentTarget.GetComponent<Runner>().Killed();
            RunnersManager.Instance.RemoveRunner(_currentTarget.gameObject);
            GameManager.Instance.RunnerKilled();
            _lineRenderer.enabled = false;
            _currentTarget = null;
            if(_lookForTargetCoroutine == null)
            {
                _lookForTargetCoroutine = StartCoroutine(TryGetNearestRunner());
            }
        }
    }

    private IEnumerator CheckIfTargetOutOfSight()
    {
        while (true)
        {
            if (_currentTarget != null)
            {
                if (Vector3.Distance(_currentTarget.transform.position, transform.position) > _sightRadius + 5 || _currentTarget.GetComponent<Runner>().GetBrightnessValue() < 100)
                {
                    LeanTween.cancel(gameObject);
                    yield return new WaitForSeconds(0.05f);
                    StopCoroutine(_aimCoroutine);
                    _currentTarget = null;
                    if(_lookForTargetCoroutine == null)
                    {
                        _lookForTargetCoroutine = StartCoroutine(TryGetNearestRunner());
                    }
                    _lineRenderer.enabled = false;
                    _checkIfTargetInSightCoroutine = null;
                    yield break;
                }
            }
            yield return null;
        }
    }
}
