﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public InputState CurrentInputState = InputState.Default;
	public Collider EndPoint;
    [SerializeField]
    private UIManager uiManager;

	[SerializeField]
	private int deathsLimit;
    private int finishedRuns = 0;
    private int runnersKilled = 0;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

	public void RunnerKilled()
    {
        runnersKilled++;
        uiManager.ActualizeDeaths(runnersKilled);
	    CheckIfGameOver();
    }

    public void AddPoint()
    {
        finishedRuns++;
        uiManager.ActualizeScore(finishedRuns);
    }

    public void AddPoint(int runValue)
    {
        finishedRuns += runValue;
        uiManager.ActualizeScore(finishedRuns);
    }

	private void CheckIfGameOver()
	{
		if (runnersKilled >= deathsLimit)
		{
			GameOverState();
		}
	}

	private void GameOverState()
	{
        //SpawnManager.Instance.gameObject.SetActive(false);
        //RunnersObjectPoolScript.Instance.HideAllObjects();
        //RunnersObjectPoolScript.Instance.gameObject.SetActive(false);
        Camera.main.GetComponent<AudioSource>().Stop();
        AudioSystem.Instance.StopAll();
		EndPoint.enabled = false;
		GameOverManager.Instance.SetUpGameOver(finishedRuns);
		SniperTargetManager.Instance.HideSnipers();
		GameOverManager.Instance.GameOverCamera.gameObject.SetActive(true);
		uiManager.GameOverUI.SetActive(true);
		Camera.main.gameObject.SetActive(false);
	}
}

public enum InputState
{
    ApplyPowerUp,
    Default
}
