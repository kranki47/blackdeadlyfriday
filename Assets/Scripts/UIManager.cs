﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textScore;
    [SerializeField]
    private TextMeshProUGUI textKills;

	public GameObject GameOverUI;


	public void ActualizeScore(int score)
    {
        textScore.text = "Cash: " + score.ToString() + "$";
    }

    public void ActualizeDeaths(int deaths)
    {
        textKills.text = "Deaths: " + deaths.ToString();
    }
}
