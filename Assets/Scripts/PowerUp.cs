﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPowerUp", menuName = "New Power Up", order = 1)]
public class PowerUp : ScriptableObject
{
    public bool IsCollectable;
    public bool IsSelectable;
    public bool UsedOnRunners;
    public PowerUpType PowerUpType;
    public Sprite PowerUpSprite;
}

public enum PowerUpType
{
    Chainlight,
    PowerCut,
    Flashbang,
    AdrenalineRush,
    LazyRush,
    Misdirection,
    DropItems
}
