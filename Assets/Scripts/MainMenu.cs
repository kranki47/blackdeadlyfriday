﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            audioSource.Play();
        }
    }

    public void Play ()
    {
	    Debug.Log("playbutton");
        SceneManager.LoadScene(1);
    }
	
	public void Exit ()
    {
	    Debug.Log("Exitbutton");
		Application.Quit();
    }
}
