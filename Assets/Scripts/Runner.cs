﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.UI;

public class Runner : MonoBehaviour
{
    //[SerializeField]
    //private GameObject pointLight;
    [SerializeField]
    private float lightCooldown = 1.0f;
    [SerializeField]
    private Renderer fairyLights;
    [SerializeField]
    private GameObject[] runnersPrefabs;
	[SerializeField]
	private StolenItem[] runnerStolenItemsLeftHand;
	[SerializeField]
	private StolenItem[] runnerStolenItemsRightHand;
	private GameObject currentRunner;
    private NavMeshAgent navMeshAgent;
    private int brightness = 100;
    private bool isTripping = false;
    public float speed = 6f;
    private RunnerState currentRunnerState = RunnerState.Alive;
    private StolenItemSize currentStolenItem = StolenItemSize.Empty;
    private StolenItem stolenItemLeftHand;
	private StolenItem stolenItemRightHand;
	private Transform currentDestination;
    private Transform defaultDestination;

	private int stolenItemsValue;
	[SerializeField] private TextMeshProUGUI priceValueText;

    void Start ()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        currentDestination = RunnersManager.Instance.GetRandomDestination();
        defaultDestination = currentDestination;
        navMeshAgent.speed = speed;
    }

    public void OnEnable()
    {
	    foreach (GameObject prefab in runnersPrefabs)
	    {
		    prefab.SetActive(false);
	    }
	    int randomPrefabIndex = Random.Range(0, runnersPrefabs.Length - 1);
	    currentRunner = runnersPrefabs[randomPrefabIndex];
	    currentRunner.SetActive(true);

	    foreach (StolenItem item in runnerStolenItemsLeftHand)
	    {
		    item.gameObject.SetActive(false);
	    }
	    foreach (StolenItem item in runnerStolenItemsRightHand)
	    {
		    item.gameObject.SetActive(false);
	    }

	    int leftHandItemIndex = Random.Range(0, runnerStolenItemsLeftHand.Length - 1);
	    stolenItemLeftHand = runnerStolenItemsLeftHand[leftHandItemIndex];
	    stolenItemLeftHand.gameObject.SetActive(true);
	    stolenItemsValue = stolenItemLeftHand.itemPrice;

	    if (stolenItemLeftHand.itemSize > StolenItemSize.Big)
	    {
		    int rightHandItemIndex = Random.Range(0, runnerStolenItemsRightHand.Length - 1);
		    stolenItemRightHand = runnerStolenItemsRightHand[rightHandItemIndex];
		    stolenItemRightHand.gameObject.SetActive(true);
		    stolenItemsValue += stolenItemRightHand.itemPrice;
	    }

	    priceValueText.text = stolenItemsValue + "$"; ;

		tag = "Runner";
        if (navMeshAgent)
        {
            navMeshAgent.speed = speed;
            currentDestination = defaultDestination;
        }
    }

    void Update()
    {
        if (navMeshAgent.enabled)
        {
            navMeshAgent.SetDestination(currentDestination.position);
        }

        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    StartCoroutine(ActivateAdrenaline());
        //}
        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    StartCoroutine(ActivateLazyRush());
        //}
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    StartCoroutine(ActivateMisdirection());
        //}
        //if (Input.GetKeyDown(KeyCode.F))
        //{
        //    StartCoroutine(Fall());
        //}
    }

    void OnMouseDown()
    {
        if (brightness == 100)
        {
            if (GameManager.Instance.CurrentInputState == InputState.Default)
            {
                TurnLightOff();
            }
            else if(GameManager.Instance.CurrentInputState == InputState.ApplyPowerUp && PowerUpSystem.Instance.CurrentPowerUp.UsedOnRunners)
            {
                RunnersManager.Instance.TickOutline(false);
                PowerUpSystem.Instance.ActivatePowerUp(PowerUpSystem.Instance.CurrentPowerUp, gameObject);
                GameManager.Instance.CurrentInputState = InputState.Default;
            }
        }
        else if(!isTripping)
        {
            isTripping = true;
            StartCoroutine(Fall());
        }
    }

    public void Killed()
    {
        LeanTween.moveY(gameObject, transform.position.y - 2, 3f);
        GetComponent<Animator>().SetTrigger("Dead");
        GetComponent<CapsuleCollider>().enabled = false;
        fairyLights.gameObject.SetActive(false);

        LeanTween.value(0, -1.6f, 5f).setOnUpdate((float v) => priceValueText.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, v));
        navMeshAgent.enabled = false;
        tag = "DeadRunner";
    }

    public void SetRandomNotUsedRunner()
    {
        int value = Random.Range(0, runnersPrefabs.Length - 1);
            currentRunner.SetActive(false);
            currentRunner = runnersPrefabs[value];
            currentRunner.SetActive(true);
    }

    public IEnumerator Fall()
    {
        if(tag == "Runner")
        {
            GetComponent<Animator>().SetTrigger("Fall");
            yield return new WaitForSeconds(0.4f);
            navMeshAgent.speed -= 3;
            yield return new WaitForSeconds(1f);
            SetRandomNotUsedRunner();
            navMeshAgent.speed += 2;
            yield return new WaitForSeconds(1.35f);
            navMeshAgent.speed = speed;
            isTripping = false;
        }
    }

    public IEnumerator ActivateAdrenaline()
    {
        if (tag == "Runner")
        {
            navMeshAgent.speed += 8;
            yield return new WaitForSeconds(1.5f);
            navMeshAgent.speed = speed;
        }
    }
    public IEnumerator ActivateLazyRush()
    {
        if (tag == "Runner")
        {
            navMeshAgent.speed -= 4;
            yield return new WaitForSeconds(1.5f);
            navMeshAgent.speed = speed;
        }
    }

    public NavMeshAgent GetNavMeshAgent()
    {
        return navMeshAgent;
    }

    public IEnumerator ActivateMisdirection()
    {
        if (tag == "Runner")
        {
            Transform previousDestination = currentDestination;
            currentDestination = SpawnManager.Instance.spawnPoints[Random.Range(0, SpawnManager.Instance.spawnPoints.Length - 1)];
            yield return new WaitForSeconds(2.5f);
            currentDestination = previousDestination;
        }

    }

    public void TurnLightOff()
    {
        fairyLights.materials[0].DisableKeyword("_EMISSION");
        fairyLights.materials[1].DisableKeyword("_EMISSION");
        fairyLights.materials[2].DisableKeyword("_EMISSION");
        fairyLights.materials[3].DisableKeyword("_EMISSION");
        brightness = 0;
        currentRunner.layer = LayerMask.NameToLayer("Default");
        if (isActiveAndEnabled)
        {
            StartCoroutine("TurnLightOn");
        }
    }

    public IEnumerator TurnLightOn()
    {
        if (isTripping)
        {
            yield return new WaitUntil(() => isTripping == false);
        }
        yield return new WaitForSeconds(lightCooldown);
        fairyLights.gameObject.SetActive(true);
        currentRunner.layer = LayerMask.NameToLayer("Characters");
        fairyLights.materials[0].EnableKeyword("_EMISSION");
        fairyLights.materials[1].EnableKeyword("_EMISSION");
        fairyLights.materials[2].EnableKeyword("_EMISSION");
        fairyLights.materials[3].EnableKeyword("_EMISSION");
        brightness = 100;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EndPoint")
        {
            gameObject.SetActive(false);
            RunnersManager.Instance.RemoveRunner(gameObject);
            GameManager.Instance.AddPoint(stolenItemsValue);
        }
    }

    public int GetBrightnessValue()
    {
        return brightness;
    }
}

public enum StolenItemSize
{
	Gigantic,
    Grand,
    Big,
    Medium,
    Small,
    Tiny,
    Empty
}

public enum RunnerState
{
    Alive,
    Zombie,
    Dead
}