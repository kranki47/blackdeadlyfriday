﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpPanel : MonoBehaviour {

    [SerializeField] private PowerUpButton _powerUpButtonPrefab;

	public void AddPowerUpButton(PowerUp powerUp)
    {
        var button = Instantiate(_powerUpButtonPrefab, transform);
        button.PowerUp = powerUp;
    }

    public void RemovePowerUpButton()
    {
        if(transform.childCount != 0)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
    }

    public void Start()
    {
        PowerUpSystem.Instance.PowerUpPicked += OnPowerUpPicked;
        PowerUpSystem.Instance.PowerUpUsed += OnPowerUpUsed;
    }

    public void OnPowerUpPicked(PowerUp powerUp)
    {
        if (powerUp.IsCollectable)
        {
            AddPowerUpButton(powerUp);
        }
    }

    public void OnPowerUpUsed()
    {
        RemovePowerUpButton();
    }
}
