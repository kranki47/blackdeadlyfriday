﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpawnManager : MonoBehaviour
{

    public static SpawnManager Instance;

    public float spawnTime = 3f;
    public float dropYOffset = 7f;
    public Transform[] spawnPoints;
    public PowerUpDrop PowerUpDropPrefab;

    public void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
        StartCoroutine(SpawnPickupCoroutine());
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        SpawnRandomPickUp();
    //    }
    //}

    void Spawn()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        GameObject runner = RunnersObjectPoolScript.Instance.GetPooledObject();
        RunnersManager.Instance.runners.Add(runner.GetComponent<Runner>());
        runner.transform.position = spawnPoints[spawnPointIndex].position;
        runner.transform.rotation = spawnPoints[spawnPointIndex].rotation;
        runner.SetActive(true);
        runner.GetComponent<Animator>().SetTrigger("Running");
        runner.GetComponent<CapsuleCollider>().enabled = true;
        runner.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        runner.GetComponentInChildren<TextMeshProUGUI>().fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, 0);
        StartCoroutine(runner.GetComponent<Runner>().TurnLightOn());
    }

    public void SpawnRandomPickUp()
    {
        PowerUpDrop drop = Instantiate(PowerUpDropPrefab);
        var random = RunnersManager.Instance.GetRandomRunner();
        if(random == null)
        {
            random = RunnersManager.Instance.GetRandomRunner();
        }
        drop.transform.position = random.transform.position;
        drop.transform.position = new Vector3(drop.transform.position.x, drop.transform.position.y + dropYOffset, drop.transform.position.z);
    }

    private IEnumerator SpawnPickupCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(4, 11));
            SpawnRandomPickUp();
        }
    }
}
