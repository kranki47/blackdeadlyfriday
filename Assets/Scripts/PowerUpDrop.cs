﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpDrop : MonoBehaviour
{
    public PowerUp[] PowerUps;
    public Vector3 ScaleBounceValue;
    [SerializeField]
    private float _dropLifetime = 2.0f;
    private bool _isOnGround = false;

    public void Awake()
    {
        StartCoroutine(StartRotatingInAir());
    }

	private void OnMouseDown()
	{
		GetComponent<Collider>().enabled = false;
		PickUp();
	}

	public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Ground" && !_isOnGround)
        {
            _isOnGround = true;
            LeanTween.scale(gameObject, ScaleBounceValue, 0.2f).setOnComplete(() => { LeanTween.scale(gameObject, Vector3.one, 0.2f); }).setEaseOutSine();
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            StartCoroutine(RemoveDrop());
        }
    }

    private void PickUp()
    {
        if (PowerUpSystem.Instance.CanPickUpPowerUp)
        {
            LeanTween.moveY(gameObject, transform.position.y + 2.5f, 0.8f).setEaseOutExpo().setOnComplete(() => LeanTween.scale(gameObject, Vector3.zero, 0.2f).setOnComplete(() => PowerUpSystem.Instance.PowerUpPicked(PowerUps[Random.Range(0, PowerUps.Length - 1)])));
        }
    }

    private IEnumerator StartRotatingInAir()
    {
        while (!_isOnGround)
        {
            transform.RotateAroundLocal(new Vector3(0.4f, -0.2f, 0.65f), 16);
            yield return null;
        }
    }

    private IEnumerator RemoveDrop()
    {
        yield return new WaitForSeconds(_dropLifetime);
        Destroy(gameObject);
    }
}
