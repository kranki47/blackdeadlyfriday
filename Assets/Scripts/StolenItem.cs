﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class StolenItem : MonoBehaviour
{
	public StolenItemSize itemSize;
	public int itemPrice;
	public GameObject Prefab;
}
