﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnersManager : MonoBehaviour
{
    public static RunnersManager Instance;
    public List<Runner> runners = new List<Runner>();

    public AudioClip[] VoiceActs;

    [SerializeField]
    private Transform[] destinations;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void Start()
    {
        StartCoroutine(PlayVoiceActs());
    }
    public IEnumerator PlayVoiceActs()
    {
        while (true)
        {
            AudioSystem.Instance.PlayOneTime(VoiceActs[Random.Range(0, VoiceActs.Length)], Channel.Voice);
            yield return new WaitForSeconds(Random.Range(4, 9));
        }
    }
    public void TickOutline(bool value)
    {
        for (int i = 0; i < runners.Count; i++)
        {
            runners[i].GetComponent<Outline>().enabled = value;
        }
    }

    public Transform GetRandomDestination()
    {
        int destinationIndex = Random.Range(0, destinations.Length -1);
        Transform currentPoint = destinations[destinationIndex];
        return currentPoint;
    }

    public GameObject GetRandomRunner()
    {
        int value = Random.Range(0, runners.Count - 1);
        for (int i = value; i >= 0; i--)
        {
            if(runners.Count - 1 >= i)
            {
                return runners[i].gameObject;
            }
        }
        return null;
    }

    public void RemoveRunner(GameObject runner)
    {
        runners.Remove(runner.GetComponent<Runner>());
    }
}
